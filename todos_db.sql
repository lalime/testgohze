-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 26, 2021 at 03:07 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todos_db`
--
CREATE DATABASE IF NOT EXISTS `todos_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `todos_db`;

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
  `task_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `history`
--

TRUNCATE TABLE `history`;
--
-- Dumping data for table `history`
--

INSERT INTO `history` (`task_id`, `status`, `created_at`) VALUES
(1, 0, '2021-05-26'),
(2, 2, '2021-05-26'),
(3, 1, '2021-05-26'),
(4, 2, '2021-05-26'),
(5, 2, '2021-05-26'),
(1, 0, '2021-05-26'),
(2, 2, '2021-05-26'),
(3, 1, '2021-05-26'),
(4, 2, '2021-05-26'),
(5, 2, '2021-05-26');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(190) DEFAULT NULL,
  `description` text NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `author` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `tasks`
--

TRUNCATE TABLE `tasks`;
--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `title`, `description`, `end_date`, `status`, `author`, `created_at`) VALUES
(1, 'Future Response Developer', 'Suscipit veritatis aspernatur adipisci error tempora commodi sunt officia. Animi perspiciatis est facilis. Accusantium velit odio officia consectetur veniam dolorem voluptatem corrupti. Repellendus aut labore minima ex.', '2021-10-02 00:00:00', 0, 1, '2021-05-26 11:35:16'),
(2, 'Senior Optimization Supervisor', 'Molestiae aut rerum minus porro amet officiis magnam et. Omnis aut et consequatur. Occaecati et adipisci id voluptas laboriosam dolorem ut enim vel. Dolorem esse rem. Tempore hic alias quo.', '2022-01-08 00:00:00', 2, 1, '2021-05-26 11:46:44'),
(3, 'Direct Interactions Director', 'Amet ea et rem et nisi aut reiciendis quidem rerum. Rem eos quod reiciendis in ex esse qui. Est ut perferendis accusantium consectetur et velit omnis non voluptatem. Non sed dolore magni ea dolores qui accusantium. Dolore vero animi consequuntur illum nesciunt. Omnis quia ea accusamus accusamus sequi autem.', '2021-04-29 00:00:00', 1, 1, '2021-05-26 12:32:40'),
(4, 'Investor Program Representative', 'Amet id dolorem esse non aliquam asperiores rerum atque ducimus. Rerum sit molestiae. Vel eaque esse culpa.', '2020-10-15 00:00:00', 2, 1, '2021-05-26 12:32:56'),
(5, 'Regional Branding Technician', 'Veniam tempora aspernatur doloribus similique. Quo dicta temporibus omnis. Voluptatem cumque ut doloribus quod. Soluta modi qui est aliquid velit ab. Molestiae et rerum maxime ipsam eligendi id voluptas. Sunt consequatur quis animi expedita praesentium rem qui.', '2021-05-05 00:00:00', 2, 1, '2021-05-26 12:33:00'),
(6, 'Customer Markets Manager', 'Deleniti qui expedita et magnam. Sint nisi unde unde accusamus et. Ut ex et sint beatae qui facere asperiores adipisci.', '2022-04-25 00:00:00', 2, 1, '2021-05-26 14:06:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(151) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
