var bindFormSubmitEvent = () => {
    // bind click to submit button
    let task_form = document.getElementById('task_form');
    if (task_form !== null) {
        task_form.addEventListener("submit", function(event){
            event.preventDefault();
            let action = task_form.getAttribute('action');
    
            // Get form data
            doSendAjax(task_form, action);
            return false;
        });
    }
}

var bindHistoryFilterEvent = () => {
    // bind click to submit button
    let form = document.getElementById('history_form')
    if (form) {
        form.addEventListener("submit", function(event){
            event.preventDefault();
            
            let action = form.getAttribute('action');

            // Get form data

            doFilterTasksAjax(form, action);
            return false;
        });
    }
}

var bindBackupEvent = () => {
    // bind click to submit button
    let backup_form = document.getElementById('backup_form')
    if (backup_form) {
        backup_form.addEventListener("submit", function(event){
            event.preventDefault();

            let action = backup_form.getAttribute('action');

            // Get form data
            doBackupAjax(backup_form, action);
            return false;
        });
    }
}

/**
 *  Submit tasks filter via ajax
 * @param {*} form 
 * @param {*} action 
 */
var doFilterTasksAjax  = function (form, action)
{
    // Get form data
    var form_data = new FormData(form);

    let buttonSubmit = document.getElementById("display_data");
    buttonSubmit.type = 'reset';
    buttonSubmit.disabled = true;
    console.log(form_data);

    // Post form data
    var xhr = new XMLHttpRequest();
    xhr.open('POST', action , true);
    xhr.onload = function ()
    {
        buttonSubmit.type = 'submit';
        buttonSubmit.disabled = false;

        if (xhr.status === 200)
        {
            // Get the response
            var data = JSON.parse(xhr.response);
            // Check the success status
            if (data.success === true) {
                // Output a success message
                var succesSpan = document.getElementById('history-body');
                succesSpan.innerHTML = data.data;
            } else {
                // Output error information
                var errorSpan = document.getElementById('notif-error');
                succesSpan.innerHTML = data.message; 
                succesSpan.style.display = 'block'; 
            }
        } else {
            // Output error information
            var errorSpan = document.getElementById('notif-error');
            succesSpan.innerHTML = xhr.status + " - " + xhr.statusText; 
            succesSpan.style.display = 'block';
        }
                
    };
    xhr.upload.addEventListener("progress", function (evt)
    {
        // if (evt.lengthComputable)
        // {
        //     var width = Math.round((evt.loaded / evt.total) * 100);
        //     progress_bar.style.width = width + '%';
        //     loading_text.innerHTML = width * 1 + '%';
        // }
    }, false);
    xhr.onerror = function ()
    {
        buttonSubmit.type = 'submit';
        buttonSubmit.disabled = false;

        // Output error information
        var errorSpan = document.getElementById('notif-error');
        succesSpan.innerHTML = xhr.status + " - " + xhr.statusText; 
        succesSpan.style.display = 'block';
    };
    xhr.send(form_data);
} // End of the doFilterTasksAjax method

/**
 *  Submit new task via ajax
 * @param {*} form 
 * @param {*} action 
 */
var doSendAjax  = function (form, action)
{
    // Get form data
    var form_data = new FormData(form);
    console.log(form_data);

    // Post form data
    var xhr = new XMLHttpRequest();
    xhr.open('POST', action , true);
    xhr.onload = function ()
    {
        if (xhr.status === 200)
        {
            // Get the response
            var data = JSON.parse(xhr.response);
            // Check the success status
            if (data.success === true) {
                // Output a success message
                var succesSpan = document.getElementById('notif-success');
                succesSpan.innerHTML = data.message; 
                succesSpan.style.display = 'block'; 

                form.reset();
            } else {
                // Output error information
                var errorSpan = document.getElementById('notif-error');
                succesSpan.innerHTML = data.message; 
                succesSpan.style.display = 'block'; 
            }
        } else {
            // Output error information
            toastr['error'](xhr.status + " - " + xhr.statusText);
        }
                
    };
    xhr.upload.addEventListener("progress", function (evt)
    {
        if (evt.lengthComputable)
        {
            var width = Math.round((evt.loaded / evt.total) * 100);
            progress_bar.style.width = width + '%';
            loading_text.innerHTML = width * 1 + '%';
        }
    }, false);
    xhr.onerror = function ()
    {
        // Output error information
        toastr['error'](xhr.status + " - " + xhr.statusText);
    };
    xhr.send(form_data);
} // End of the doSendAjax method

/**
 *  Backup record for the day
 * @param {*} form 
 * @param {*} action 
 */
var doBackupAjax  = function (form, action)
{
    // Get form data
    var form_data = new FormData(form);
    console.log(form_data);

    // Post form data
    var xhr = new XMLHttpRequest();
    xhr.open('POST', action , true);
    xhr.onload = function ()
    {
        if (xhr.status === 200)
        {
            // Get the response
            var data = JSON.parse(xhr.response);
            // Check the success status
            if (data.success === true) {
                // Output a success message
                alert('Backup Done!')
            } else {
                // Output error information
                alert('Error : ' + data.message)
            }
        } else {
            // Output error information
            alert('Error : ' + xhr.status + " - " + xhr.statusText)
        }
                
    };
    xhr.upload.addEventListener("progress", function (evt)
    {
    }, false);
    xhr.onerror = function ()
    {
        // Output error information
        alert('Error : ' + xhr.status + " - " + xhr.statusText)
    };
    xhr.send(form_data);
} // End of the sendForm method