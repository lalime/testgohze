<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'config/functions.php';

$pdo = getPdoConnection();    

if (isset($_POST['action']))  {
    $action = $_POST['action'];

    switch ($action) {
        case 'save_task':
                $data = [
                    'title' => filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING),
                    'description' => filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING),
                    'end_date' => date ('Y-m-d H:i:s', strtotime(filter_input(INPUT_POST, 'end_date'))),
                    'status' => filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT),
                    'author' => filter_input(INPUT_POST, 'author', FILTER_VALIDATE_INT),
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                // die(var_dump(filter_input(INPUT_POST, 'end_date'), $data));
            
                $sql = "INSERT INTO tasks (title, description, end_date, status, author, created_at) VALUES (:title, :description, :end_date, :status, :author, :created_at)";
                $stmt= $pdo->prepare($sql);
                $stmt->execute($data);
            
                jsonResponse(200, true, 'saved successfuly !');
            break;

        case 'display_archive':
            
                // Add created at condition
                $where['created_at'] = date ('Y-m-d', strtotime(filter_input(INPUT_POST, 'filter_date')));

                $tasks = getTaskHistory($where);

                ob_start();

                include 'views/task-rows.php';
                // Get table form buffer
                $out1 = ob_get_clean();

                jsonResponse(200, true, 'ok', $out1);
            break;

        case 'backup_tasks':

            
                archiveDailyTasks();

                jsonResponse(200, true, "Done!");
            break;
        
        default:
            # code...
            break;
    }
    
}
die('-1');