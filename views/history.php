<?php ?>


<section id="history-task">
    <div class="section-header">
        <h1> Tasks History</h1>
    </div>

    <div class="_row task-row">
        <div class="text-right filter-form">
            <form action="process-ajax.php" method="POST" id="history_form">
                <input type="date" id="filter_date" name="filter_date" placeholder="Enter date" required/>
                <input type="hidden" name="action" value="display_archive">
                <input type="submit" id="display_data" value="Afficher">
            </form>
        </div>
        <p>
            <span id="notif-error"></span>
        </p>
        <div style="overflow-x:auto;">
            <table > 
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>title</th>
                        <th>Description</th>
                        <th>Created at</th>
                        <th>End at</th>
                    </tr>
                </thead>
                <tbody id="history-body">
                    
                    <tr>
                        <td colspan="4">
                            <h2>Sorry!</h2>
                            <p>No record found!</p>
                        </td>
                    </tr>
                    
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>

    </div>
</section>