<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<section id="list-task">
    <div class="section-header">
        <h1> Login / Register</h1>
    </div>
    <div class="_row task-row">
        <section class="auth">

            <div class="section-header">
                <p>
                    <span id="notif-success"></span>
                    <span id="notif-error"></span>
                </p>
            </div>
            <div class="row">
                <div class="column">

                    <div class="form">
                        <form action="process-ajax.php" method="POST" id="login_form">
                            <label for="username">Nom</label>
                            <input type="text" id="username" name="username" placeholder="Enter name" required>

                            <label for="password">Mot de passe :</label>
                            <input type="text" id="password" name="password" placeholder="Enter password" required>

                            <input type="hidden" name="action" value="login_user">
                            <input type="submit" id="login" value="Login">
                        </form>
                    </div>
                </div>
                <div class="column">

                    <div class="form">
                        <form action="process-ajax.php" method="POST" id="register_form">
                            <label for="usr_name">Nom</label>
                            <input type="text" id="usr_name" name="username" placeholder="Enter name" required>

                            <label for="usr_password">Mot de passe :</label>
                            <input type="text" id="usr_password" name="password" placeholder="Enter password" required>

                            <label for="c_password">Confirmer Mot de passe :</label>
                            <input type="text" id="c_password" name="c_password" placeholder="Enter same password" required>

                            <input type="hidden" name="action" value="register_user">
                            <input type="submit" id="register" value="Regiser">
                        </form>
                    </div>

                </div>
            </div>
        </section>
    </div>
</section>