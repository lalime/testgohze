<?php if (!empty($tasks) && count($tasks)) : ?>
    <?php foreach ($tasks as $task) : ?>
        <tr>
            <td>
            <?php if ($task->status == 1) : ?>
                <span class="success text-center text-bold">Complete</span>
            <?php elseif($task->status == 2) : ?>
                <span class="current text-center text-bold">Processing</span>
            <?php else : ?>
                <span class="waiting text-center text-bold">Pending</span>
            <?php endif; ?>
            </td>
            <td><?php echo $task->title; ?></td>
            <td><?php echo $task->description; ?></td>
            <td><?php echo $task->created_at; 
                        ?></td>
            <td><?php echo $task->end_date; ?></td>
        </tr>
    <?php endforeach; ?>
<?php else : ?>
    <tr>
        <td colspan="4">
            <h2>Sorry! <?php echo strtotime('now'); ?> </h2>
            <p>No record found!</p>
        </td>
    </tr>
<?php endif; ?>