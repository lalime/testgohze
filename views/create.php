<?php
?>
<section id="add-task">
    <div class="section-header">
        <h1> Add task</h1>
        <p>
            <span id="notif-success"></span>
            <span id="notif-error"></span>
        </p>
    </div>

    <div class="form">
        <form action="process-ajax.php" method="POST" id="task_form">
            <label for="title">Title</label>
            <input type="text" id="title" name="title" placeholder="Task label.." required>

            <label for="description">Description</label>
            <textarea type="text" id="description" name="description" rows="10" required placeholder="A few words to describe task.."></textarea>

            <label for="end_date">End Date</label>
            <input type="date" id="end_date" name="end_date" placeholder="Task end date" required />

            <label for="status">Status</label>
            <select id="status" name="status">
                <option value="0" selected>Pending</option>
                <option value="1">Current</option>
                <option value="2">Complete</option>
            </select>

            <input type="hidden" name="action" value="save_task">
            <input type="hidden" name="author" value="1">
            <input type="submit" id="saveTask" value="Submit">
        </form>
    </div>
</section>