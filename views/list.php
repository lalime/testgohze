<?php

require 'config/functions.php';

$where = [];

if (isset($_GET['status'])) {
    if (is_numeric($_GET['status']))
        $where['status'] = $_GET['status'];
}
// var_dump($where); die;
$tasks = getTaskList($where);

?>
<section id="list-task">
    <div class="section-header">
        <h1> Daily tasks</h1>
    </div>
    <div class="_row task-row">
        <div class="text-right">

            <form action="process-ajax.php" method="POST" id="backup_form">
                <input type="hidden" name="action" value="backup_tasks">
                <input type="submit" style="width: 200px;" id="backup_status" value="Cloturer">
            </form>
            <ul class="filter">
                <li><a href="?view=lists&status=all">Tous</a></li>
                <li><a href="?view=lists&status=2">Processing</a></li>
                <li><a href="?view=lists&status=1">Complete</a></li>
                <li><a href="?view=lists&status=0">Pending</a></li>
            </ul>
        </div>
        <div style="overflow-x:auto;">
            <table>
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>title</th>
                        <th>Description</th>
                        <th>Created at</th>
                        <th>End at</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($tasks) && count($tasks)) : ?>
                        <?php foreach ($tasks as $task) : ?>
                            <tr>
                                <td>
                                <?php if ($task->status == 1) : ?>
                                    <span class="success text-center text-bold">Complete</span>
                                <?php elseif($task->status == 2) : ?>
                                    <span class="current text-center text-bold">Processing</span>
                                <?php else : ?>
                                    <span class="waiting text-center text-bold">Pending</span>
                                <?php endif; ?>
                                </td>
                                <td><?php echo $task->title; ?></td>
                                <td><?php echo $task->description; ?></td>
                                <td><?php echo $task->created_at; 
                                            ?></td>
                                <td><?php echo $task->end_date; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <tr>
                            <td colspan="4">
                                <h2>Sorry!</h2>
                                <p>No record found!</p>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>

    </div>
</section>