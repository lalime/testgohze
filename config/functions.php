<?php
require 'constants.php';

function getPdoConnection() {
    
    try{ 
        $conn = new \PDO("mysql:host=". HOSTNAME .";dbname=". DBNAME ."", USERNAME, PASSWORD);

        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
    }
    catch(\PDOExtension $e){
        echo $e->getMessage();
    }

    return null;
}

function jsonResponse($code, $success, $message = "", $data = []) {
    $result = array(
        'code' => $code,
        'success' => $success,
        'message' => $message,
        'data' => $data
    );
    echo json_encode($result);
    die;
}

function getTaskList($where = [], $offset = 0, $limit = 10) {
    $cond =  "";
    $pdo = getPdoConnection();

    if (count($where)) {
        foreach ($where as $key => $value) {
            if (in_array($key, ['status', 'author'])) {
                $cond .= " AND $key=$value";
            } else {
                $cond .= " AND $key='$value'";
            }
        }
    }
    
    $stmt = $pdo->query("SELECT *  FROM `tasks` WHERE 1 $cond ORDER BY id DESC LIMIT $offset,$limit");
    $tasks = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $tasks;
}

function getTaskHistory($where = [], $offset = 0, $limit = 10) {
    $cond =  "";
    $pdo = getPdoConnection();

    if (count($where)) {
        foreach ($where as $key => $value) {
            if (in_array($key, ['status', 'author'])) {
                $cond .= " AND `history`$key=$value";
            } else {
                $cond .= " AND `history`.$key='$value'";
            }
        }
    }
    $stmt = $pdo->query("SELECT DISTINCT(`history`.task_id), `tasks`.*, `history`.status FROM `history` INNER JOIN `tasks` ON `tasks`.id=`history`.task_id WHERE 1 $cond ORDER BY id DESC LIMIT $offset,$limit");
    $tasks = $stmt->fetchAll(PDO::FETCH_OBJ);

    return $tasks;
}

function archiveDailyTasks() {
    $pdo = getPdoConnection();
    
    $sql = "INSERT INTO `history` (`task_id`, `status`) SELECT `id`, `status`  FROM `tasks`";
    $pdo->prepare($sql)->execute();

    return 1;
}