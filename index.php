<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test</title>
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <div id="page">
        <div class="header">
            <a href="#default" class="logo">Test</a>
            <div class="header-right">
                <a class="<?php echo !isset($_GET['view']) || $_GET['view'] == 'lists'?'active':'' ?>" href="?view=lists">Tasks</a>
                <a class="<?php echo isset($_GET['view']) && $_GET['view'] == 'create'?'active':'' ?>" href="?view=create">Add Task</a>
                <a class="<?php echo isset($_GET['view']) && $_GET['view'] == 'history'?'active':'' ?>" href="?view=history">History</a>
                <a class="<?php echo isset($_GET['view']) && $_GET['view'] == 'login'?'active':'' ?>" href="?view=login">Login</a>
            </div>
        </div>

        <div class="container-column">

        <?php if (!isset($_GET['view']) || $_GET['view'] == 'lists') : 
            
                include 'views/list.php';
            
            elseif (isset($_GET['view']) && $_GET['view'] == 'create') :

                include 'views/create.php';

            elseif (isset($_GET['view']) && $_GET['view'] == 'history') : 
                include 'views/history.php';
                
            elseif (isset($_GET['view']) && $_GET['view'] == 'login') : 

                include 'views/login.php';
                
            endif;
            ?>
        

        </div>
        <footer class="text-center">
            <p>Author: Lalim</p>
            <p><a href="mailto:lalim@example.com">lalim@example.com</a></p>
        </footer>
    </div>
    <script src="assets/js/events.js"></script>
    <script src="assets/js/app.js"></script>

</body>

</html>